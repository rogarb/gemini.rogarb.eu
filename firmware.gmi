=> index.gmi Return to main page

# Firmware description of the Huawei HG532e ADSL router

## Obtaining the firmware
There are different ways of obtaining the firmware:
* by dumping the content of the flash chip with an external programmer (see the flash chip pinout in the hardware description part). Reading from and writing to the flash content required adding a 10 ohm resistor between the flash and the programmer grounds (a floating ground would allow reading though). See the guide (link on the main page). Be sure to dump the flash several times and check that the dumps are all the same (using i.e. md5sum or sha256sum).
* by downloading it from the internet. I couldn't find it on the official Huawei website but it is available in some third party site.
* by using a serial connection (see hardware description) and dumping the memory content from the bootloader interface. This is a bit hacky as we have to save the content of the serial communication in a text file and then process the relevant part to convert it back to actual bytes. I wrote a couple of scripts in order to streamline the process (this can be used to dump anything from the SoC address space, including register content). More on that later in a separate page.

## Firmware Content
```
*---------------*-------------*--------------*---------------------------------*
| Start address | End address | Size (bytes) | Description                     |
*---------------*-------------*--------------*---------------------------------*
| 0x0           | 0xadee      | 44526        | Bootloader: initialization code |
|               |             |              | until 0x21d8 (8664 bytes)       |
|               |             |              | followed by LZMA compressed     |
|               |             |              | payload (35862 bytes)           |
|               |             |              |                                 |
| 0xadef        | 0x10013     | 21028        | Zeroed with string content      |
|               |             |              |                                 |
| 0x10014       | 0x10fff     | 4076         | Uninitialized content (OxFF)    |
| ....          |             |              |                                 |
| 0x30100       | 0x10c0dc    | 901084       | Linux kernel (LZMA compressed)  |
|               |             |              |                                 |
| 0x10c0dc      | 0x392561    | 2647173      | rootfs (SquashFS image)         |
|               |             |              |                                 |
| 0x392561      | 0x3930df    | 2942         | Zeroed with final 32bit value   |
|               |             |              |                                 |
| 0x3930e0      | 0x3dffff    | 315167       | Uninitialized content (OxFF)    |
|               |             |              |                                 |
| 0x3e0000      | 0x3e6174    | 24948        | Unknown content                 |
|               |             |              |                                 |
| 0x3e6175      | 0x3effff    | 40586        | Uninitialized content (OxFF)    |
|               |             |              |                                 |
| 0x3f0000      | 0x3f000d    | 12           | Unidentified byte value         |
|               |             |              |                                 |
| 0x3f000e      | 0x400000    | 20           | Uninitialized content (OxFF)    |
*---------------*-------------*--------------*---------------------------------*
```

## Bootloader
The bootloader consists of 4 parts:
* some initialization code from 0x00 to 0x1260
* an LZMA decompressor/loader from 0x1260 to 0x21d8
* an LZMA compressed payload (the actual bootloader) from 0x21d8 to 0xadee
* some strings/data content from 0xadef to 0x10013

=> bootloader_experiments.gmi Some experiments with bootloader modifications

## Rootfs
The firmware seems to use a custom squashfs format: version 3.0 with LZMA compression and a swapped magic number: 0x71736873 (QSHS) instead of 0x73717368 (SQSH). 

This can be handled by the `sasquatch` program, a patched version of squashfs-tools:
=> https://github.com/devttys0/sasquatch

--------------------------------------------------------------------------------
=> index.gmi Return to main page
=> legal.gmi Legal notice

-----
content CC-BY-SA rogarb@rogarb.eu
=> https://creativecommons.org/licenses/by-sa/4.0/
